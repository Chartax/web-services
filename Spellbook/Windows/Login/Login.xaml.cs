﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Security.Cryptography;


namespace Spellbook.Windows.Login
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public bool LoginSuccessful = false;


        public Login()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            var username = loginBox.Text;
            var password = passwordBox.Password;

            var sha = new SHA512CryptoServiceProvider();

            try
            {
                var db = new SpellbookClassesDataContext();
                var usr = from u in db.users where u.userName == username select u;
                var user = usr.First();

                var salt = user.salt.ToArray();
                var saltString = Encoding.ASCII.GetString(salt);
                var data = Encoding.ASCII.GetBytes(password + saltString);

                var hash = sha.ComputeHash(data);
                var storedHash = user.hash.ToArray();

                if (hash.SequenceEqual(storedHash))
                {
                    LocalApplication.USER_NAME = user.userName;
                    LocalApplication.USER_ID = user.id;
                    MessageBox.Show("Login successful!");
                    this.LoginSuccessful = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Could not login, please check your username/password and try again.");
                }
            }
            catch
            {
                MessageBox.Show("Could not login, please check your username/password and try again.");
            }
            
        }

        private void SignUpButton_Click(object sender, RoutedEventArgs e)
        {
            var username = loginBox.Text;
            var password = passwordBox.Password;

            var sha = new SHA512CryptoServiceProvider();
            var rng = new RNGCryptoServiceProvider();

            var salt = new byte[32];
            rng.GetBytes(salt);
            var saltString = Encoding.ASCII.GetString(salt);

            var data = Encoding.ASCII.GetBytes(password + saltString);

            var hash = sha.ComputeHash(data);

            var user = new user();

            user.hash = hash;
            user.salt = salt;
            user.userName = username;

            bool errors = false;

            try
            {
                var db = new SpellbookClassesDataContext();
                db.users.InsertOnSubmit(user);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                errors = true;
                MessageBox.Show("There was a problem with your account creation. Try a different username.");
            }

            if (!errors)
            {
                MessageBox.Show("Thank you for creating an account! Now press Login.");
            }
        }
    }
}
