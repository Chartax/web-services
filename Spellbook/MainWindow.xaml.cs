﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace Spellbook
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LoadDecks();
        }

        public void LoadDecks()
        {
            Uri serviceUri = new Uri("http://localhost:11328/API/deck");
            WebClient downloader = new WebClient();
            downloader.OpenReadCompleted += new OpenReadCompletedEventHandler(downloader_OpenReadCompleted);
            downloader.OpenReadAsync(serviceUri);
        }

        void downloader_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Stream responseStream = e.Result;
                var streamReader = new StreamReader(responseStream);
                var serializer = new JsonSerializer();
                var json = new JsonTextReader(streamReader);
                var data = serializer.Deserialize<List<deck>>(json);
                foreach (var deck in data)
                {
                    decks.Items.Add(deck.deckName);
                }
            }
        }


        private void ImportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var window = new JSONImport();
            window.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var newDeck = new NewDeck();
            newDeck.Show();
        }
    }
}
