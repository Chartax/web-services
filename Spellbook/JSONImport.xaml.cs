﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace Spellbook
{
    /// <summary>
    /// Interaction logic for JSONImport.xaml
    /// </summary>
    public partial class JSONImport : Window
    {
        public JSONImport()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                var fileName = openFileDialog.FileName;
                FileNameBox.Text = fileName;
            }
        }

        private void Import(string filepath)
        {
            var cards = new List<JSONCard>();
            if (filepath == "")
            {
                MessageBox.Show("You must select a file to import.");
            }
            else if (!System.IO.File.Exists(filepath))
            {
                {
                    MessageBox.Show("File specified does not appear to exist.");
                }
            }
            else
            {
                var reader = new System.IO.StreamReader(filepath);
                var json = new Newtonsoft.Json.JsonTextReader(reader);
                var serializer = new Newtonsoft.Json.JsonSerializer();

                var databaseContext = new SpellbookClassesDataContext();

                try
                {
                    var extraList = new List<JSONCard>();

                    var objects = serializer.Deserialize<Dictionary<string, JSONSet>>(json);
                    using (databaseContext)
                    {
                        
                        var setList = new List<set>();
                        var cardList = new List<card>();
                        foreach (var set in objects)
                        {
                            var jsonSet = set.Value;
                            var newSet = new set();

                            newSet.code = jsonSet.code;
                            newSet.block = jsonSet.block;
                            newSet.border = jsonSet.border;
                            newSet.gathererCode = jsonSet.gathererCode;
                            newSet.name = jsonSet.name;
                            newSet.oldCode = jsonSet.oldCode;
                            newSet.onlineOnly = jsonSet.onlineOnly;
                            newSet.releaseDate = jsonSet.releaseDate;
                            newSet.type = jsonSet.type;

                            // newSet.cards = jsonSet.cards;

                            setList.Add(newSet);
                        }
                         
                        databaseContext.sets.InsertAllOnSubmit<set>(setList);
                        databaseContext.SubmitChanges();
                        
                        foreach (var set in objects)
                        {
                            var dbSet = from s in databaseContext.sets where s.code == set.Value.code select s;

                            foreach (var card in set.Value.cards)
                            {
                                var jsonCard = card;
                                var newCard = new card();
                                newCard.layout = jsonCard.layout;
                                newCard.name = jsonCard.name;
                                newCard.manaCost = jsonCard.manaCost;
                                newCard.cmc = jsonCard.cmc;
                                newCard.type = jsonCard.type;
                                newCard.text = jsonCard.text;
                                newCard.flavor = jsonCard.flavor;
                                newCard.artist = jsonCard.artist;
                                newCard.number = jsonCard.number;
                                newCard.power = jsonCard.power;
                                newCard.toughness = jsonCard.toughness;
                                newCard.loyalty = jsonCard.loyalty;
                                newCard.multiverseid = jsonCard.multiverseid;
                                newCard.imageName = jsonCard.imageName;
                                newCard.watermark = jsonCard.watermark;
                                newCard.border = jsonCard.border;
                                newCard.timeshifted = jsonCard.timeshifted;
                                newCard.rarity = jsonCard.rarity;
                                newCard.hand = jsonCard.hand;
                                newCard.life = jsonCard.life;
                                newCard.reserved = jsonCard.reserved;
                                newCard.releaseDate = jsonCard.releaseDate;
                                newCard.set = dbSet.First();

                                // names
                                if (jsonCard.names != null)
                                {
                                    extraList.Add(jsonCard);
                                }
                                // colors
                                if (jsonCard.colors != null)
                                {
                                    extraList.Add(jsonCard);
                                }
                                // subtypes
                                if (jsonCard.subtypes != null)
                                {
                                    extraList.Add(jsonCard);
                                }
                                // supertypes
                                if (jsonCard.supertypes != null)
                                {
                                    extraList.Add(jsonCard);
                                }
                                // types
                                if (jsonCard.types != null)
                                {
                                    extraList.Add(jsonCard);
                                }
                                // variations
                                if (jsonCard.variations != null)
                                {
                                    extraList.Add(jsonCard);
                                }

                                cardList.Add(newCard);
                            }
                        }
                        databaseContext.cards.InsertAllOnSubmit<card>(cardList);
                        databaseContext.SubmitChanges();

                        foreach (var card in extraList)
                        {
                            var dbCard = from c in databaseContext.cards where c.name == card.name select c;
                            var dataCard = dbCard.First();

                            if (card.names != null)
                            {
                                foreach (var name in card.names)
                                {
                                    name cardName = new name();
                                    cardName.cardId = dataCard.id;
                                    cardName.name1 = name;
                                    databaseContext.names.InsertOnSubmit(cardName);
                                }
                            }
                            
                            if (card.colors != null)
                            {
                                foreach (var name in card.colors)
                                {
                                    color cardName = new color();
                                    cardName.cardId = dataCard.id;
                                    cardName.name = name;
                                    databaseContext.colors.InsertOnSubmit(cardName);
                                }
                            }
                            
                            if (card.variations != null)
                            {
                                foreach (var name in card.variations)
                                {
                                    variation cardName = new variation();
                                    cardName.cardId = dataCard.id;
                                    cardName.variation1 = name;
                                    databaseContext.variations.InsertOnSubmit(cardName);
                                }
                            }
                            
                            if (card.types != null)
                            {
                                foreach (var name in card.types)
                                {
                                    type cardName = new type();
                                    cardName.cardId = dataCard.id;
                                    cardName.type1 = name;
                                    databaseContext.types.InsertOnSubmit(cardName);
                                }
                            }
                            
                            if (card.subtypes != null)
                            {
                                foreach (var name in card.subtypes)
                                {
                                    subtype cardName = new subtype();
                                    cardName.cardId = dataCard.id;
                                    cardName.subtype1 = name;
                                    databaseContext.subtypes.InsertOnSubmit(cardName);
                                }
                            }
                            
                            if (card.supertypes != null)
                            {
                                foreach (var name in card.supertypes)
                                {
                                    supertype cardName = new supertype();
                                    cardName.cardId = dataCard.id;
                                    cardName.supertype1 = name;
                                    databaseContext.supertypes.InsertOnSubmit(cardName);
                                }
                            }
                            
                        }

                        databaseContext.SubmitChanges();
                    }

                }
                catch (Newtonsoft.Json.JsonReaderException e)
                {
                    MessageBox.Show("File provided does not appear to be valid JSON.");
                    System.Console.WriteLine("Error: " + e.Message);
                }
            }
        }

        async private void ImportButton_Click(object sender, RoutedEventArgs e)
        {
            var text = FileNameBox.Text;
            ImportProgress.IsIndeterminate = true;
            await Task.Run(() => Import(text));
            ImportProgress.IsIndeterminate = false;
        }
    }
}
