﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spellbook
{
    class JSONCard
    {
        public string layout;
        public string name;
        public List<string> names;
        public string manaCost;
        public decimal? cmc;
        public List<string> colors;
        public string type;
        public List<string> supertypes;
        public List<string> types;
        public List<string> subtypes;
        public string rarity;
        public string text;
        public string flavor;
        public string artist;
        public string number;
        public string power;
        public string toughness;
        public int loyalty;
        public int multiverseid;
        public List<int> variations;
        public string imageName;
        public string watermark;
        public string border;
        public bool timeshifted;
        public int hand;
        public int life;
        public bool reserved;
        public string releaseDate;
    }
}
