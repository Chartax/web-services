﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using Newtonsoft.Json;

namespace Spellbook
{
    /// <summary>
    /// Interaction logic for NewDeck.xaml
    /// </summary>
    public partial class NewDeck : Window
    {
        public NewDeck()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // http://localhost:11328/API/deck
            // POST
            // deckName == whatever
            // userId == current user
            string result = "";
            var deck = new deck();
            deck.deckName = deckNameBox.Text;
            deck.userId = LocalApplication.USER_ID;

            var deckString = JsonConvert.SerializeObject(deck);
            

            using (var client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/json");
                result = client.UploadString("http://localhost:11328/API/deck", "POST", deckString);
            }

            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
