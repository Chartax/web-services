﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spellbook
{
    class JSONSet
    {
        public string name;
        public string code;
        public string gathererCode;
        public string oldCode;
        public string releaseDate;
        public string border;
        public string type;
        public string block;
        public bool onlineOnly;
        public List<object> booster;
        public List<JSONCard> cards;
    }
}
