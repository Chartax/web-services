﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SpellbookAPI.Models;

namespace SpellbookAPI.Controllers
{
    public class DeckController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<deck> Get()
        {
            var db = new SpellbookClassesDataContext();
            var decks = from d in db.decks select d;
            return decks.ToList();
        }

        // GET api/<controller>/5
        public deck Get(int id)
        {
            var db = new SpellbookClassesDataContext();
            var decks = from d in db.decks where d.id == id select d;
            return decks.First();
        }

        // POST api/<controller>
        public void Post([FromBody]deck value)
        {
            var db = new SpellbookClassesDataContext();
            db.decks.InsertOnSubmit(value);
            db.SubmitChanges();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]deck value)
        {
            var db = new SpellbookClassesDataContext();
            var targetDeck = from d in db.decks where d.id == id select d;
            var change = targetDeck.First();
            change = value;
            db.SubmitChanges();
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            var db = new SpellbookClassesDataContext();
            var targetDeck = from d in db.decks where d.id == id select d;
            var delete = targetDeck.First();
            db.decks.DeleteOnSubmit(delete);
        }
    }
}