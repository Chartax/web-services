﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SpellbookAPI.Models;

namespace SpellbookAPI.Controllers
{
    
    public class UserController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<user> Get()
        {
            var usersList = new List<user>();
            var db = new SpellbookClassesDataContext();
            var users = from u in db.users select u;
            return users.ToList();
        }

        // GET api/<controller>/5
        public user Get(int id)
        {
            var db = new SpellbookClassesDataContext();
            var users = from u in db.users where u.id == id select u;
            return users.AsEnumerable().First();
        }

        // POST api/<controller>
        public void Post([FromBody]user newUser)
        {
            var db = new SpellbookClassesDataContext();
            db.users.InsertOnSubmit(newUser);
            db.SubmitChanges();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]user value)
        {
            var db = new SpellbookClassesDataContext();
            var usr = from u in db.users where u.id == id select u;
            usr.First().hash = value.hash;
            usr.First().salt = value.salt;
            usr.First().decks = value.decks;
            db.SubmitChanges();
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            var db = new SpellbookClassesDataContext();
            var usr = from u in db.users where u.id == id select u;
            db.users.DeleteOnSubmit(usr.First());
        }
    }
}